# Contributing

Contributions are welcome, and they are greatly appreciated! Every little bit
helps, and credit will always be given.

You can contribute in many ways:

## Types of Contributions

### Report Bugs

Report bugs at [Gitlab](https://gitlab.com/redkeep/codebuild-packer/-/issues).

If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

###Fix Bugs

Look through the Gitlb issues for bugs. Anything tagged with "bug" is open to 
whoever wants to implement it.

### Implement Features

Look through the Gitlab issues for features. Anything tagged with "enhancement"
and "help wanted" is open to whoever wants to implement it.

### Write Documentation

Packer scripts could always use more documentation, whether as part of the 
official Packer docs, in docstrings, or even on the web in blog posts, articles, 
and such.

### Submit Feedback

The best way to send feedback is to file an issue in the 
[Gitlab Project](https://gitlab.com/redkeep/codebuild-packer/-/issues).

If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.
* Remember that this is a volunteer-driven project, and that contributions
  are welcome :)

## Get Started!

Ready to contribute? Here's how to set up `common` for local development.

1. Clone `common` locally::

    $ git clone https://gitlab.com/redkeep/codebuild-packer.git

2. Create a branch for local development::

    $ git checkout -b name-of-your-bugfix-or-feature

   Now you can make your changes locally.

3. Commit your changes and push your branch to the 
[repo](https://gitlab.com/redkeep/codebuild-packer.git):

    $ git add .
    $ git commit -m "Your detailed description of your changes."
    $ git push origin name-of-your-bugfix-or-feature

4. Submit a pull request through the repo website.

## Pull Request Guidelines

Before you submit a pull request, check that it meets these guidelines:

1. The pull request should have been tested thoroughly.
2. If the pull request adds functionality, the docs should be updated. Put
   your new functionality into a function with a docstring, and add the
   feature to the list in README.md and HISTORY.md.

## Tips
N/A

## Deploying

A reminder for the maintainers on how to deploy.
Make sure all your changes are committed (including an entry in HISTORY.md).
Then run:

$ bump2version <patch> # possible: [major / minor / patch](https://semver.org/)
$ git push
$ git push --tags
