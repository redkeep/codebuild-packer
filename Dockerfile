# codebuild-packer v0.1.0
FROM ubuntu:18.04

RUN \
    apt-get update && apt-get -y upgrade && \
    apt-get install -y zip wget curl && \
    curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add - && \
    sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" && \
    sudo apt-get update && sudo apt-get install packer
