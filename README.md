# Packer Codebuild v0.1
Packer docker image to be used in CI/CD pipeline for Codebuild.

## ! Don't use this package; Use the Default Hashicorp package instead !

`name: hashicorp/packer:latest`
